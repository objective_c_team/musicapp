//
//  HomeRootPageViewController.h
//  MusicApp
//
//  Created by Samvel Pahlevanyan on 2/22/17.
//  Copyright © 2017 WeDoApps. All rights reserved.
//

#import "REFrostedViewController.h"

@interface HomeRootPageViewController  : REFrostedViewController
-(void)initilizatiaTapGesturel;
@end
