//
//  HomeRootPageViewController.m
//  MusicApp
//
//  Created by Samvel Pahlevanyan on 2/22/17.
//  Copyright © 2017 WeDoApps. All rights reserved.
//
#import "LeftMenuViewController.h"

#import "HomeRootPageViewController.h"

@interface HomeRootPageViewController ()
@property (strong, nonatomic)  UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation HomeRootPageViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomePageNavViewController"];
    self.menuViewController = [LeftMenuViewController getShearedLeftMenu];
}

-(void)initilizatiaTapGesturel {
    [self.view removeGestureRecognizer:_tapGestureRecognizer];
    _tapGestureRecognizer = nil;
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapReceived:)];
    [self.view addGestureRecognizer:_tapGestureRecognizer];
}

-(void)tapReceived:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view removeGestureRecognizer:_tapGestureRecognizer];
    _tapGestureRecognizer = nil;
}

@end
