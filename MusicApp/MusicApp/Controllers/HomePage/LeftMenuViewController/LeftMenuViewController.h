//
//  LeftMenuViewController.h
//  MusicApp
//
//  Created by Samvel Pahlevanyan on 2/22/17.
//  Copyright © 2017 WeDoApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuViewController : UIViewController
+(instancetype)getShearedLeftMenu;
@end
