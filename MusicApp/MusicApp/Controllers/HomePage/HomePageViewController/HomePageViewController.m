//
//  HomePageViewController.m
//  MusicApp
//
//  Created by Samvel Pahlevanyan on 2/22/17.
//  Copyright © 2017 WeDoApps. All rights reserved.
//
#import "HomePageViewController.h"

@interface HomePageViewController ()
@property (assign, nonatomic) BOOL leftBarPresent;

@end

@implementation HomePageViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
}
#pragma mark
#pragma mark - clickButtons

- (IBAction)showMenu:(UIBarButtonItem *)sender {
    
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
    _leftBarPresent = YES;
    [UIView animateWithDuration:0.45 animations:^{
        self.view.frame = CGRectMake(self.view.frame.size.width/2,
                                     self.view.frame.origin.y,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
        [self.view layoutIfNeeded];
    }];
}

-(void)closeMenu {
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(0,
                                     self.view.frame.origin.y,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
        [self.view layoutIfNeeded];
    }];
}


@end

