//
//  HomePageViewController.h
//  MusicApp
//
//  Created by Samvel Pahlevanyan on 2/22/17.
//  Copyright © 2017 WeDoApps. All rights reserved.
//
#import "REFrostedViewController.h"
#import <UIKit/UIKit.h>

@interface HomePageViewController : UIViewController

-(IBAction)showMenu:(id)sender;
-(void)closeMenu;

@end
